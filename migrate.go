package qry

import (
	"context"
	"embed"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

func Migrate(db *pgxpool.Pool, fs embed.FS, files []string) {
	err := Transaction(db, func(tx pgx.Tx) error {
		sql := "CREATE TABLE IF NOT EXISTS migration (version TEXT UNIQUE NOT NULL)"
		_, err := tx.Exec(context.Background(), sql)
		if err != nil {
			return err
		}
		for _, filename := range files {
			if exists(tx, filename) {
				continue
			}
			migrate(tx, fs, filename)
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
}

func migrate(tx pgx.Tx, fs embed.FS, filename string) {
	sql, err := fs.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	ctx := context.Background()
	if _, err := tx.Exec(ctx, string(sql)); err != nil {
		panic(err)
	}
	if _, err := tx.Exec(ctx, "INSERT INTO migration(version) VALUES($1)", filename); err != nil {
		panic(err)
	}
}

func exists(tx pgx.Tx, version string) bool {
	var e bool
	err := tx.QueryRow(
		context.Background(),
		`SELECT EXISTS(SELECT version FROM migration WHERE version=$1)`,
		version,
	).Scan(&e)
	if err != nil {
		panic(err)
	}
	return e
}
