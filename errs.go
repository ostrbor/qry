package qry

import (
	"errors"
	"fmt"
	"reflect"
)

var errNil = errors.New("nil is not allowed")

func errKind(a, b reflect.Kind) error {
	return fmt.Errorf("unexpected kind: %s != %s", a, b)
}
