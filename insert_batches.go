package qry

import (
	"fmt"
	"github.com/jackc/pgtype/pgxtype"
	"reflect"
)

func InsertInBatches(db pgxtype.Querier, ptr interface{}, batchSize int) (err error) {
	v, err := dereference(ptr)
	if err != nil {
		return err
	}
	t := v.Type()
	if t.Kind() != reflect.Slice {
		return errKind(t.Kind(), reflect.Slice)
	}
	if err := isStruct(t.Elem()); err != nil {
		return err
	}
	if v.Len() == 0 {
		return fmt.Errorf("slice len: %d == 0", v.Len())
	}
	for _, group := range groups(sliceItems(v), batchSize) {
		err := insertMany(db, group)
		if err != nil {
			return err
		}
	}
	return nil
}

// groups([1, 2, 3], 2) -> [[1, 2], [3]]
func groups(items []reflect.Value, maxSize int) [][]reflect.Value {
	length := len(items)
	if length == 0 {
		return nil
	}
	if maxSize <= 0 || maxSize > length {
		maxSize = length
	}

	groups := make([][]reflect.Value, 1)
	idx := 0
	g := make([]reflect.Value, 0)
	groups[idx] = g
	for _, item := range items {
		if len(g) == maxSize {
			idx++
			g = make([]reflect.Value, 0)
			groups = append(groups, g)
		}
		g = append(g, item)
		groups[idx] = g
	}
	return groups
}
