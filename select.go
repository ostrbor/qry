package qry

import (
	"context"
	"fmt"
	"github.com/jackc/pgtype/pgxtype"
	"reflect"
	"strings"
)

func MustSelect(db pgxtype.Querier, ptr interface{}, where string, params ...interface{}) {
	if err := Select(db, ptr, where, params...); err != nil {
		panic(err)
	}
}

func Select(db pgxtype.Querier, ptr interface{}, where string, params ...interface{}) (err error) {
	v, err := dereference(ptr)
	if err != nil {
		return err
	}
	switch v.Kind() {
	case reflect.Struct:
		return selectOne(db, v, where, params...)
	case reflect.Slice:
		if err := isStruct(v.Type().Elem()); err != nil {
			return err
		}
		if v.Len() != 0 {
			return fmt.Errorf("slice len: %d != 0", v.Len())
		}
		return selectMany(db, v, where, params...)
	}
	return fmt.Errorf("invalid kind: %s", v.Kind())
}

func selectOne(db pgxtype.Querier, s reflect.Value, where string, params ...interface{}) error {
	fields, err := structFields(s)
	if err != nil {
		return err
	}
	sql := fmt.Sprintf(
		"SELECT %s FROM %s %s",
		strings.Join(columns(fields), ","), table(fields), where,
	)

	row := db.QueryRow(context.Background(), sql, params...)
	return row.Scan(pointers(fields)...)
}

func selectMany(db pgxtype.Querier, slice reflect.Value, where string, params ...interface{}) error {
	itemType := slice.Type().Elem()
	fields, err := structFields(reflect.New(itemType))
	if err != nil {
		return err
	}
	sql := fmt.Sprintf(
		"SELECT %s FROM %s %s",
		strings.Join(columns(fields), ","), table(fields), where,
	)

	rows, err := db.Query(context.Background(), sql, params...)
	if err != nil {
		return err
	}
	for rows.Next() {
		i := reflect.New(itemType)
		fields, err = structFields(i.Elem())
		if err != nil {
			return err
		}
		if err := rows.Scan(pointers(fields)...); err != nil {
			rows.Close()
			return err
		}
		slice.Set(reflect.Append(slice, i.Elem()))
	}
	return rows.Err()
}
