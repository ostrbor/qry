package qry

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

func Transaction(db *pgxpool.Pool, f func(tx pgx.Tx) error) (err error) {
	ctx := context.Background()
	tx, err := db.Begin(ctx)
	if err != nil {
		return fmt.Errorf("failed db.Begin: %s", err)
	}

	defer func() {
		tx.Rollback(ctx)
	}()

	if err = f(tx); err != nil {
		return fmt.Errorf("failed f: %s", err)
	}

	if e := tx.Commit(ctx); e != nil {
		err = fmt.Errorf("failed tx.Commit: %s", err)
	}

	return err
}
