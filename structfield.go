package qry

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
)

const (
	columnKey = "column"
	tableKey  = "table"
	serialOpt = "serial"
)

type structField struct {
	Name string
	Ptr  interface{}

	// from tag:
	Table  string
	Column string
	Serial bool
}

func structFields(structOrStructPtr reflect.Value) (f []structField, err error) {
	s := reflect.Indirect(structOrStructPtr)
	if s.Kind() != reflect.Struct {
		return nil, errKind(s.Kind(), reflect.Struct)
	}

	tableCount := 0
	serialCount := 0
	for i := 0; i < s.NumField(); i++ {
		sf := s.Type().Field(i)
		column, serial := parse(sf.Tag)
		if column == "" {
			continue
		}
		table := sf.Tag.Get(tableKey)
		if table != "" {
			tableCount++
		}
		if serial {
			serialCount++
		}
		f = append(f, structField{
			Name:   sf.Name,
			Ptr:    s.Field(i).Addr().Interface(),
			Table:  table,
			Column: column,
			Serial: serial,
		})
	}
	if len(f) == 0 {
		return nil, errors.New("missing 'column' in tags")
	}
	if tableCount == 0 {
		return nil, errors.New("missing 'table' in tags")
	} else if tableCount > 1 {
		return nil, fmt.Errorf("table key: %d > 1", tableCount)
	}
	if serialCount > 1 {
		return nil, fmt.Errorf("serial option: %d > 1", serialCount)
	}
	return f, nil
}

func parse(t reflect.StructTag) (column string, serial bool) {
	c := t.Get(columnKey)
	if c == "" {
		return "", false
	}
	column = strings.Split(c, ",")[0]
	serial = strings.Contains(c, serialOpt)
	return column, serial
}

func splitSerials(fields []structField) (rest []structField, sers []structField) {
	for _, f := range fields {
		if f.Serial {
			sers = append(sers, f)
			continue
		}
		rest = append(rest, f)
	}
	return rest, sers
}

func columns(fields []structField) (res []string) {
	for _, f := range fields {
		res = append(res, f.Column)
	}
	return res
}

func pointers(fields []structField) (res []interface{}) {
	for _, f := range fields {
		res = append(res, f.Ptr)
	}
	return res
}

func values(fields []structField) (res []interface{}) {
	for _, f := range fields {
		res = append(res, reflect.ValueOf(f.Ptr).Elem().Interface())
	}
	return res
}

func table(f []structField) string {
	for _, f := range f {
		if f.Table != "" {
			return f.Table
		}
	}
	return ""
}
