package qry

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"strings"
)

func Truncate(db *pgxpool.Pool, tables []string) {
	sql := fmt.Sprintf(`TRUNCATE TABLE %s RESTART IDENTITY CASCADE`, strings.Join(tables, ","))
	_, err := db.Exec(context.Background(), sql)
	if err != nil {
		panic(err)
	}
}
