package qry

import (
	"reflect"
)

func dereference(v interface{}) (ptr reflect.Value, err error) {
	if v == nil {
		return ptr, errNil
	}
	p := reflect.ValueOf(v)
	if p.Kind() != reflect.Ptr {
		return ptr, errKind(p.Kind(), reflect.Ptr)
	}
	return p.Elem(), nil
}

func isStruct(t reflect.Type) (err error) {
	if t.Kind() != reflect.Struct {
		return errKind(t.Kind(), reflect.Struct)
	}
	return nil
}

func sliceItems(slice reflect.Value) (items []reflect.Value) {
	for i := 0; i < slice.Len(); i++ {
		items = append(items, slice.Index(i))
	}
	return items
}
