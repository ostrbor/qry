package qry

import (
	"context"
	"fmt"
	"github.com/jackc/pgtype/pgxtype"
	"reflect"
	"strings"
)

func MustInsert(db pgxtype.Querier, ptr interface{}) {
	if err := Insert(db, ptr); err != nil {
		panic(err)
	}
}

func Insert(db pgxtype.Querier, ptr interface{}) (err error) {
	v, err := dereference(ptr)
	if err != nil {
		return err
	}
	switch v.Kind() {
	case reflect.Struct:
		return insertOne(db, v)
	case reflect.Slice:
		if err := isStruct(v.Type().Elem()); err != nil {
			return err
		}
		if v.Len() == 0 {
			return fmt.Errorf("slice len: %d == 0", v.Len())
		}
		return insertMany(db, sliceItems(v))
	}
	return fmt.Errorf("invalid kind: %s", v.Kind())
}

func insertOne(db pgxtype.Querier, s reflect.Value) (err error) {
	fields, err := structFields(s)
	if err != nil {
		return err
	}
	insFields, sers := splitSerials(fields)
	vals := values(insFields)
	sql := fmt.Sprintf(
		"INSERT INTO %s (%s) VALUES %s",
		table(fields), strings.Join(columns(insFields), ","), sqlValues(len(vals), 0),
	)

	if len(sers) != 0 {
		sql += fmt.Sprintf(" RETURNING %s", sers[0].Column)
		return db.QueryRow(context.Background(), sql, vals...).Scan(sers[0].Ptr)
	}
	_, err = db.Exec(context.Background(), sql, vals...)
	return err
}

func insertMany(db pgxtype.Querier, items []reflect.Value) (err error) {
	schemaFields, err := structFields(reflect.New(items[0].Type()))
	if err != nil {
		return err
	}
	insertFields, _ := splitSerials(schemaFields)
	sql := fmt.Sprintf(
		"INSERT INTO %s (%s)",
		table(schemaFields), strings.Join(columns(insertFields), ","),
	)

	var vals []interface{}
	var serials []structField
	for _, s := range items {
		fields, err := structFields(s)
		if err != nil {
			return err
		}
		insFields, sers := splitSerials(fields)
		vals = append(vals, values(insFields)...)
		serials = append(serials, sers...)
	}

	sql += fmt.Sprintf(" VALUES %s", sqlValues(len(insertFields), len(items)))
	if len(serials) == 0 {
		_, err = db.Exec(context.Background(), sql, vals...)
		return err
	}

	sql += fmt.Sprintf(" RETURNING %s", serials[0].Column)
	rows, err := db.Query(context.Background(), sql, vals...)
	if err != nil {
		return err
	}

	ptrs := pointers(serials)
	for i := 0; rows.Next(); i++ {
		if i+1 > len(ptrs) {
			rows.Close()
			return fmt.Errorf("%d rows > %d serials", i+1, len(ptrs))
		}
		if err := rows.Scan(ptrs[i]); err != nil {
			rows.Close()
			return err
		}
	}
	return rows.Err()
}
