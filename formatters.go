package qry

import (
	"fmt"
	"strings"
)

// Returns $ placeholders for VALUES clause.
// -> ($1,$2) | ($1,$2),($3,$4)
func sqlValues(rowLen, rowCount int) string {
	if rowCount < 1 {
		rowCount = 1
	}

	var rows []string
	row := make([]string, 0)
	for i := 1; i <= rowLen*rowCount; i++ {
		row = append(row, fmt.Sprintf("$%d", i))
		if len(row) == rowLen {
			rows = append(rows, fmt.Sprintf("(%s)", strings.Join(row, ",")))
			row = make([]string, 0)
		}
	}
	return strings.Join(rows, ",")
}
